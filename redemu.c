#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <SDL2/SDL.h>

#define SCREEN_WIDTH 768
#define SCREEN_HEIGHT 720

const int XLINE = 256;
const int YLINE = 240;
SDL_Window *window = NULL;
SDL_Renderer *renderer = NULL;
SDL_Texture *texture = NULL;
uint32_t screen[XLINE * YLINE] = { 0 };

char *i_names[256] = {
  "BRK", "ORA", "---", "---", "---", "ORA", "ASL", "---",   "PHP", "ORA", "ASL", "---", "---", "ORA", "ASL", "---",
  "BPL", "ORA", "---", "---", "---", "ORA", "ASL", "---",   "CLC", "ORA", "---", "---", "---", "ORA", "ASL", "---",
  "JSR", "AND", "---", "---", "BIT", "AND", "ROL", "---",   "PLP", "AND", "ROL", "---", "BIT", "AND", "ROL", "---",
  "BMI", "AND", "---", "---", "---", "AND", "ROL", "---",   "SEC", "AND", "---", "---", "---", "AND", "ROL", "---",
  "RTI", "EOR", "---", "---", "---", "EOR", "LSR", "---",   "PHA", "EOR", "LSR", "---", "JMP", "EOR", "LSR", "---",
  "BVC", "EOR", "---", "---", "---", "EOR", "LSR", "---",   "CLI", "EOR", "---", "---", "---", "EOR", "LSR", "---",
  "RTS", "ADC", "---", "---", "---", "ADC", "ROR", "---",   "PLA", "ADC", "ROR", "---", "JMP", "ADC", "ROR", "---",
  "BVS", "ADC", "---", "---", "---", "ADC", "ROR", "---",   "SEI", "ADC", "---", "---", "---", "ADC", "ROR", "---",

  "---", "STA", "---", "---", "STY", "STA", "STX", "---",   "DEY", "---", "TXA", "---", "STY", "STA", "STX", "---",
  "BCC", "STA", "---", "---", "STY", "STA", "STX", "---",   "TYA", "STA", "TXS", "---", "---", "STA", "---", "---",
  "LDY", "LDA", "LDX", "---", "LDY", "LDA", "LDX", "---",   "TAY", "LDA", "TAX", "---", "LDY", "LDA", "LDX", "---",
  "BCS", "LDA", "---", "---", "LDY", "LDA", "LDX", "---",   "CLV", "LDA", "TSX", "---", "LDY", "LDA", "LDX", "---",
  "CPY", "CMP", "---", "---", "CPY", "CMP", "DEC", "---",   "INY", "CMP", "DEX", "---", "CPY", "CMP", "DEC", "---",
  "BNE", "CMP", "---", "---", "---", "CMP", "DEC", "---",   "CLD", "CMP", "---", "---", "---", "CMP", "DEC", "---",
  "CPX", "SBC", "---", "---", "CPX", "SBC", "INC", "---",   "INX", "SBC", "NOP", "---", "CPX", "SBC", "INC", "---",
  "BEQ", "SBC", "---", "---", "---", "SBC", "INC", "---",   "SED", "SBC", "---", "---", "---", "SBC", "INC", "---",
};

uint8_t i_length[256] = {
  2, 2, 0, 0, 0, 2, 2, 0,   1, 2, 1, 0, 0, 3, 3, 0,
  2, 2, 0, 0, 0, 2, 2, 0,   1, 3, 0, 0, 0, 3, 3, 0,
  3, 2, 0, 0, 2, 2, 2, 0,   1, 2, 1, 0, 3, 3, 3, 0,
  2, 2, 0, 0, 0, 2, 2, 0,   1, 3, 0, 0, 0, 3, 3, 0,
  1, 2, 0, 0, 0, 2, 2, 0,   1, 2, 1, 0, 3, 3, 3, 0,
  2, 2, 0, 0, 0, 2, 2, 0,   1, 3, 0, 0, 0, 3, 3, 0,
  1, 2, 0, 0, 0, 2, 2, 0,   1, 2, 1, 0, 3, 3, 3, 0,
  2, 2, 0, 0, 0, 2, 2, 0,   1, 3, 0, 0, 0, 3, 3, 0,

  0, 2, 0, 0, 2, 2, 2, 0,   1, 0, 1, 0, 3, 3, 3, 0,
  2, 2, 0, 0, 2, 2, 2, 0,   1, 3, 1, 0, 0, 3, 0, 0,
  2, 2, 2, 0, 2, 2, 2, 0,   1, 2, 1, 0, 3, 3, 3, 0,
  2, 2, 0, 0, 2, 2, 2, 0,   1, 3, 1, 0, 3, 3, 3, 0,
  2, 2, 0, 0, 2, 2, 2, 0,   1, 2, 1, 0, 3, 3, 3, 0,
  2, 2, 0, 0, 0, 2, 2, 0,   1, 3, 0, 0, 0, 3, 3, 0,
  2, 2, 0, 0, 2, 2, 2, 0,   1, 2, 1, 0, 3, 3, 3, 0,
  2, 2, 0, 0, 0, 2, 2, 0,   1, 3, 0, 0, 0, 3, 3, 0,

};

typedef struct CPUState {
  uint8_t A; //accumulator
  uint8_t X;
  uint8_t Y;
  uint8_t SP;
  uint16_t PC;
  uint8_t status;
  uint8_t *memory;
} CPUState;

typedef struct PPUState {
  uint8_t *memory;
} PPUState;

CPUState* CPUInit(void) {
  srand(time(NULL));
  CPUState *state = calloc(1, sizeof(CPUState));
  state->memory = malloc(0x10000); //64KB address space
  state->status = 0x00;
  return state;
}

PPUState* PPUInit(void) {
  PPUState *state = calloc(1, sizeof(PPUState));
  state->memory = malloc(0x4000); //16KB address space
  return state;
}

void CPUSetFlag(uint8_t *status, char flag) {
  switch (flag) {
    case "c": *status |= 0x01; break;
    case "z": *status |= 0x02; break;
    case "i": *status |= 0x04; break;
    case "d": *status |= 0x08; break;
    case "b": *status |= 0x10; break;
    case "v": *status |= 0x40; break;
    case "n": *status |= 0x80; break;
  }
}

void CPUClearFlag(uint8_t *status, char flag) {
  switch (flag) {
    case "c": *status -= *status & 0x01; break;
    case "z": *status -= *status & 0x02; break;
    case "i": *status -= *status & 0x04; break;
    case "d": *status -= *status & 0x08; break;
    case "b": *status -= *status & 0x10; break;
    case "v": *status -= *status & 0x40; break;
    case "n": *status -= *status & 0x80; break;
  }
}

int ins_x0(CPUState *state, uint8_t *code) {
  int cycle = 0;
  switch((code[0] & 0xf0) >> 4) {
    case 0x0:
      //BRK
      //push PC onto the stack
      CPUSetFlag(&state->status, "b");
      break;
    case 0x1:
      //BPL rel
      if ((state->status & 0x80) == 0x00) {
        state->PC += (signed char)code[1];
      }
      break;
    case 0x2:
      //JSR abs
      //push state->PC onto the stack
      state->PC = (((uint16_t)code[2]) << 8) + code[1];
      break;
    case 0x3:
      //BMI rel
      if ((state->status & 0x80) != 0x00) {
        state->PC += (signed char)code[1];
      }
      break;
    case 0x4:
      //RTI
      //return
      break;
    case 0x5:
      //BVC rel
      if ((state->status & 0x40) == 0x00) {
        state->PC += (signed char)code[1];
      }
      break;
    case 0x6:
      //RTS
      //return and set flags
      break;
    case 0x7:
      //BVS rel
      if ((state->status & 0x40) != 0x00) {
        state->PC += (signed char)code[1];
      }
      break;
    case 0x8: break;
    case 0x9:
      //BCC rel
      if ((state->status & 0x01) == 0x00) {
        state->PC += (signed char)code[1];
      }
      break;
    case 0xa:
      //LDY imm
      state->Y = code[1];
      if (state->Y == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->Y & 0x80 != 0x00) { CPUSetFlag(state->status, "n"); } else { CPUClearFlag(state->status, "n"); }
      break;
    case 0xb:
      //BCS rel
      if ((state->status & 0x01) != 0x00) {
        state->PC += (signed char)code[1];
      }
      break;
    case 0xc:
      //CPY imm
      //compare
      break;
    case 0xd:
      //BNE rel
      if ((state->status & 0x02) == 0x00) {
        state->PC += (signed char)code[1];
      }
      break;
    case 0xe:
      //CPX imm
      break;
    case 0xf:
      //BEQ rel
      if ((state->status & 0x02) != 0x00) {
        state->PC += (signed char)code[1];
      }
      break;
  }
  return cycle;
}

int ins_x1(CPUState *state, uint8_t *code) {
  int cycle = 0;
  switch((code[0] & 0xf0) >> 4) {
    case 0x0:
      //ORA idx
      uint8_t nib_h = state->memory[(uint16_t)(state->X + code[1]) + 1];
      uint8_t nib_l = state->memory[(uint16_t)(state->X + code[1])];
      state->A |= state->memory[((uint16_t)nib_h << 8) + (uint16_t)nib_l];
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      break;
    case 0x1:
      //ORA idy
      uint8_t nib_h = state->memory[(uint16_t)code[1] + 1];
      uint8_t nib_l = state->memory[(uint16_t)code[1]];
      state->A |= state->memory[((uint16_t)nib_h << 8) + (uint16_t)nib_l + state->Y];
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      break;
    case 0x2:
      //AND idx
      uint8_t nib_h = state->memory[(uint16_t)(state->X + code[1]) + 1];
      uint8_t nib_l = state->memory[(uint16_t)(state->X + code[1])];
      state->A &= state->memory[((uint16_t)nib_h << 8) + (uint16_t)nib_l];
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      break;
    case 0x3:
      //AND idy
      uint8_t nib_h = state->memory[(uint16_t)code[1] + 1];
      uint8_t nib_l = state->memory[(uint16_t)code[1]];
      state->A &= state->memory[((uint16_t)nib_h << 8) + (uint16_t)nib_l + state->Y];
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      break;
    case 0x4:
      //EOR idx
      uint8_t nib_h = state->memory[(uint16_t)(state->X + code[1]) + 1];
      uint8_t nib_l = state->memory[(uint16_t)(state->X + code[1])];
      state->A ^= state->memory[((uint16_t)nib_h << 8) + (uint16_t)nib_l];
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      break;
    case 0x5:
      //EOR idy
      uint8_t nib_h = state->memory[(uint16_t)code[1] + 1];
      uint8_t nib_l = state->memory[(uint16_t)code[1]];
      state->A ^= state->memory[((uint16_t)nib_h << 8) + (uint16_t)nib_l + state->Y];
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      break;
    case 0x6:
      //ADC idx
      uint8_t nib_h = state->memory[(uint16_t)(state->X + code[1]) + 1];
      uint8_t nib_l = state->memory[(uint16_t)(state->X + code[1])];
      state->A += state->memory[((uint16_t)nib_h << 8) + (uint16_t)nib_l] + (state->status & 0x01);
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      //carry flag
      //overflow flag
      break;
    case 0x7:
      //ADC idy
      uint8_t nib_h = state->memory[(uint16_t)code[1] + 1];
      uint8_t nib_l = state->memory[(uint16_t)code[1]];
      state->A += state->memory[((uint16_t)nib_h << 8) + (uint16_t)nib_l + state->Y] + (state->status & 0x01);
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      //carry flag
      //overflow flag
      break;
    case 0x8:
      //STA idx
      uint8_t nib_h = state->memory[(uint16_t)(state->X + code[1]) + 1];
      uint8_t nib_l = state->memory[(uint16_t)(state->X + code[1])];
      state->memory[((uint16_t)nib_h << 8) + (uint16_t)nib_l] = state->A;
      break;
    case 0x9:
      //STA idy
      uint8_t nib_h = state->memory[(uint16_t)code[1] + 1];
      uint8_t nib_l = state->memory[(uint16_t)code[1]];
      state->memory[((uint16_t)nib_h << 8) + (uint16_t)nib_l + state->Y] = state->A;
      break;
    case 0xa:
      //LDA idx
      uint8_t nib_h = state->memory[(uint16_t)(state->X + code[1]) + 1];
      uint8_t nib_l = state->memory[(uint16_t)(state->X + code[1])];
      state->A = state->memory[((uint16_t)nib_h << 8) + (uint16_t)nib_l];
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      break;
    case 0xb:
      //LDA idy
      uint8_t nib_h = state->memory[(uint16_t)code[1] + 1];
      uint8_t nib_l = state->memory[(uint16_t)code[1]];
      state->A = state->memory[((uint16_t)nib_h << 8) + (uint16_t)nib_l + state->Y];
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      break;
    case 0xc:
      //CMP idx
      uint8_t nib_h = state->memory[(uint16_t)(state->X + code[1]) + 1];
      uint8_t nib_l = state->memory[(uint16_t)(state->X + code[1])];
      uint8_t m = state->memory[((uint16_t)nib_h << 8) + (uint16_t)nib_l];
      if (state->A == m) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A >= m) { CPUSetFlag(state->status, "c"); } else { CPUClearFlag(state->status, "c"); }
      if ((state->A - m) & 0x80 != 0x00) { CPUSetFlag(state->status, "n"); } else { CPUClearFlag(state->status, "n"); }
      break;
    case 0xd:
      //CMP idy
      uint8_t nib_h = state->memory[(uint16_t)code[1] + 1];
      uint8_t nib_l = state->memory[(uint16_t)code[1]];
      uint8_t m = state->memory[((uint16_t)nib_h << 8) + (uint16_t)nib_l + state->Y];
      if (state->A == m) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A >= m) { CPUSetFlag(state->status, "c"); } else { CPUClearFlag(state->status, "c"); }
      if ((state->A - m) & 0x80 != 0x00) { CPUSetFlag(state->status, "n"); } else { CPUClearFlag(state->status, "n"); }
      break;
    case 0xe:
      //SBC idx
      uint8_t nib_h = state->memory[(uint16_t)(state->X + code[1]) + 1];
      uint8_t nib_l = state->memory[(uint16_t)(state->X + code[1])];
      state->A -= state->memory[((uint16_t)nib_h << 8) + (uint16_t)nib_l] + ~(state->status & 0x01);
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 != 0x00) { CPUSetFlag(state->status, "n"); } else { CPUClearFlag(state->status, "n"); }
      //carry flag
      //overflow flag
      break;
    case 0xf:
      //SBC idy
      uint8_t nib_h = state->memory[(uint16_t)code[1] + 1];
      uint8_t nib_l = state->memory[(uint16_t)code[1]];
      state->A -= state->memory[((uint16_t)nib_h << 8) + (uint16_t)nib_l + state->Y] + ~(state->status & 0x01);
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 != 0x00) { CPUSetFlag(state->status, "n"); } else { CPUClearFlag(state->status, "n"); }
      //carry flag
      //overflow flag
      break;
  }
  return cycle;
}

int ins_x2(CPUState *state, uint8_t *code) {
  int cycle = 0;
  switch((code[0] & 0xf0) >> 4) {
    case 0x0: break;
    case 0x1: break;
    case 0x2: break;
    case 0x3: break;
    case 0x4: break;
    case 0x5: break;
    case 0x6: break;
    case 0x7: break;
    case 0x8: break;
    case 0x9: break;
    case 0xa:
      //LDX imm
      state->X = code[1];
      if (state->X == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->X & 0x80 != 0x00) { CPUSetFlag(state->status, "n"); } else { CPUClearFlag(state->status, "n"); }
      break;
    case 0xb: break;
    case 0xc: break;
    case 0xd: break;
    case 0xe: break;
    case 0xf: break;
  }
  return cycle;
}

int ins_x3(CPUState *state, uint8_t *code) {
  int cycle = 0;
  switch((code[0] & 0xf0) >> 4) {
    case 0x0: break;
    case 0x1: break;
    case 0x2: break;
    case 0x3: break;
    case 0x4: break;
    case 0x5: break;
    case 0x6: break;
    case 0x7: break;
    case 0x8: break;
    case 0x9: break;
    case 0xa: break;
    case 0xb: break;
    case 0xc: break;
    case 0xd: break;
    case 0xe: break;
    case 0xf: break;
  }
  return cycle;
}

int ins_x4(CPUState *state, uint8_t *code) {
  int cycle = 0;
  switch((code[0] & 0xf0) >> 4) {
    case 0x0: break;
    case 0x1: break;
    case 0x2: break;
    case 0x3: break;
    case 0x4:
      //BIT zp
      if (state->memory[(uint16_t)code[1]] & state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->memory[(uint16_t)code[1]] & 0x40 != 0x00) { CPUSetFlag(state->status, "v"); } else { CPUClearFlag(state->status, "v"); }
      if (state->memory[(uint16_t)code[1]] & 0x80 != 0x00) { CPUSetFlag(state->status, "n"); } else { CPUClearFlag(state->status, "n"); }
      break;
    case 0x5: break;
    case 0x6: break;
    case 0x7: break;
    case 0x8:
      //STY zp
      state->memory[(uint16_t)code[1]] = state->Y;
      break;
    case 0x9:
      //STY zpx
      state->memory[(uint16_t)(code[1] + state->X)] = state->Y;
      break;
    case 0xa:
      //LDY zp
      state->Y = state->memory[(uint16_t)code[1]];
      if (state->Y == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->Y & 0x80 != 0x00) { CPUSetFlag(state->status, "n"); } else { CPUClearFlag(state->status, "n"); }
      break;
    case 0xb:
      //LDY zpx
      state->Y = state->memory[(uint16_t)(code[1] + state->X]];
      if (state->Y == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->Y & 0x80 != 0x00) { CPUSetFlag(state->status, "n"); } else { CPUClearFlag(state->status, "n"); }
      break;
    case 0xc:
      //CPY zp
      break;
    case 0xd: break;
    case 0xe:
      //CPX zp
      break;
    case 0xf: break;
  }
  return cycle;
}

int ins_x5(CPUState *state, uint8_t *code) {
  int cycle = 0;
  uint8_t m = state->memory[(uint16_t)code[1]];
  uint8_t mx = state->memory[(uint16_t)(code[1] + state->X)];

  switch((code[0] & 0xf0) >> 4) {
    case 0x0:
      //ORA zp
      state->A |= m;
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      break;
    case 0x1:
      //ORA zpx
      state->A |= mx;
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      break;
    case 0x2:
      //AND zp
      state->A &= m;
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      break;
    case 0x3:
      //AND zpx
      state->A &= mx;
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      break;
    case 0x4:
      //EOR zp
      state->A ^= m;
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      break;
    case 0x5:
      //EOR zpx
      state->A ^= mx;
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      break;
    case 0x6:
      //ADC zp
      state->A += m + (state->status & 0x01);
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      //carry flag
      //overflow flag
      break;
    case 0x7:
      //ADC zpx
      state->A += mx + (state->status & 0x01);
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      //carry flag
      //overflow flag
      break;
    case 0x8:
      //STA zp
      state->memory[(uint16_t)code[1]] = state->A;
      break;
    case 0x9:
      //STA zpx
      state->memory[(uint16_t)(code[1] + state->X)] = state->A;
      break;
    case 0xa:
      //LDA zp
      state->A = m;
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      break;
    case 0xb:
      //LDA zpx
      state->A = mx;
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 == 0) { CPUClearFlag(state->status, "n"); } else { CPUSetFlag(state->status, "n"); }
      break;
    case 0xc:
      //CMP zp
      if (state->A == m) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A >= m) { CPUSetFlag(state->status, "c"); } else { CPUClearFlag(state->status, "c"); }
      if ((state->A - m) & 0x80 != 0x00) { CPUSetFlag(state->status, "n"); } else { CPUClearFlag(state->status, "n"); }
      break;
    case 0xd:
      //CMP zpx
      if (state->A == mx) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A >= mx) { CPUSetFlag(state->status, "c"); } else { CPUClearFlag(state->status, "c"); }
      if ((state->A - mx) & 0x80 != 0x00) { CPUSetFlag(state->status, "n"); } else { CPUClearFlag(state->status, "n"); }
      break;
    case 0xe:
      //SBC zp
      state->A -= m + ~(state->status & 0x01);
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 != 0x00) { CPUSetFlag(state->status, "n"); } else { CPUClearFlag(state->status, "n"); }
      //carry flag
      //overflow flag
      break;
    case 0xf:
      //SBC zpx
      state->A -= mx + ~(state->status & 0x01);
      if (state->A == 0x00) { CPUSetFlag(state->status, "z"); } else { CPUClearFlag(state->status, "z"); }
      if (state->A & 0x80 != 0x00) { CPUSetFlag(state->status, "n"); } else { CPUClearFlag(state->status, "n"); }
      //carry flag
      //overflow flag
      break;
  }
  return cycle;
}

int ins_x6(CPUState *state, uint8_t *code) {
  int cycle = 0;
  switch((code[0] & 0xf0) >> 4) {
    case 0x0:
      //ASL zp
      
      break;
    case 0x1: break;
    case 0x2: break;
    case 0x3: break;
    case 0x4: break;
    case 0x5: break;
    case 0x6: break;
    case 0x7: break;
    case 0x8: break;
    case 0x9: break;
    case 0xa: break;
    case 0xb: break;
    case 0xc: break;
    case 0xd: break;
    case 0xe: break;
    case 0xf: break;
  }
  return cycle;
}

int ins_x7(CPUState *state, uint8_t *code) {
  int cycle = 0;
  switch((code[0] & 0xf0) >> 4) {
    case 0x0: break;
    case 0x1: break;
    case 0x2: break;
    case 0x3: break;
    case 0x4: break;
    case 0x5: break;
    case 0x6: break;
    case 0x7: break;
    case 0x8: break;
    case 0x9: break;
    case 0xa: break;
    case 0xb: break;
    case 0xc: break;
    case 0xd: break;
    case 0xe: break;
    case 0xf: break;
  }
  return cycle;
}

int ins_x8(CPUState *state, uint8_t *code) {
  int cycle = 0;
  switch((code[0] & 0xf0) >> 4) {
    case 0x0: break;
    case 0x1: break;
    case 0x2: break;
    case 0x3: break;
    case 0x4: break;
    case 0x5: break;
    case 0x6: break;
    case 0x7: break;
    case 0x8: break;
    case 0x9: break;
    case 0xa: break;
    case 0xb: break;
    case 0xc: break;
    case 0xd: break;
    case 0xe: break;
    case 0xf: break;
  }
  return cycle;
}

int ins_x9(CPUState *state, uint8_t *code) {
  int cycle = 0;
  switch((code[0] & 0xf0) >> 4) {
    case 0x0: break;
    case 0x1: break;
    case 0x2: break;
    case 0x3: break;
    case 0x4: break;
    case 0x5: break;
    case 0x6: break;
    case 0x7: break;
    case 0x8: break;
    case 0x9: break;
    case 0xa: break;
    case 0xb: break;
    case 0xc: break;
    case 0xd: break;
    case 0xe: break;
    case 0xf: break;
  }
  return cycle;
}

int ins_xa(CPUState *state, uint8_t *code) {
  int cycle = 0;
  switch((code[0] & 0xf0) >> 4) {
    case 0x0: break;
    case 0x1: break;
    case 0x2: break;
    case 0x3: break;
    case 0x4: break;
    case 0x5: break;
    case 0x6: break;
    case 0x7: break;
    case 0x8: break;
    case 0x9: break;
    case 0xa: break;
    case 0xb: break;
    case 0xc: break;
    case 0xd: break;
    case 0xe: break;
    case 0xf: break;
  }
  return cycle;
}

int ins_xb(CPUState *state, uint8_t *code) {
  int cycle = 0;
  switch((code[0] & 0xf0) >> 4) {
    case 0x0: break;
    case 0x1: break;
    case 0x2: break;
    case 0x3: break;
    case 0x4: break;
    case 0x5: break;
    case 0x6: break;
    case 0x7: break;
    case 0x8: break;
    case 0x9: break;
    case 0xa: break;
    case 0xb: break;
    case 0xc: break;
    case 0xd: break;
    case 0xe: break;
    case 0xf: break;
  }
  return cycle;
}

int ins_xc(CPUState *state, uint8_t *code) {
  int cycle = 0;
  switch((code[0] & 0xf0) >> 4) {
    case 0x0: break;
    case 0x1: break;
    case 0x2: break;
    case 0x3: break;
    case 0x4: break;
    case 0x5: break;
    case 0x6: break;
    case 0x7: break;
    case 0x8: break;
    case 0x9: break;
    case 0xa: break;
    case 0xb: break;
    case 0xc: break;
    case 0xd: break;
    case 0xe: break;
    case 0xf: break;
  }
  return cycle;
}

int ins_xd(CPUState *state, uint8_t *code) {
  int cycle = 0;
  switch((code[0] & 0xf0) >> 4) {
    case 0x0: break;
    case 0x1: break;
    case 0x2: break;
    case 0x3: break;
    case 0x4: break;
    case 0x5: break;
    case 0x6: break;
    case 0x7: break;
    case 0x8: break;
    case 0x9: break;
    case 0xa: break;
    case 0xb: break;
    case 0xc: break;
    case 0xd: break;
    case 0xe: break;
    case 0xf: break;
  }
  return cycle;
}

int ins_xe(CPUState *state, uint8_t *code) {
  int cycle = 0;
  switch((code[0] & 0xf0) >> 4) {
    case 0x0: break;
    case 0x1: break;
    case 0x2: break;
    case 0x3: break;
    case 0x4: break;
    case 0x5: break;
    case 0x6: break;
    case 0x7: break;
    case 0x8: break;
    case 0x9: break;
    case 0xa: break;
    case 0xb: break;
    case 0xc: break;
    case 0xd: break;
    case 0xe: break;
    case 0xf: break;
  }
  return cycle;
}

int ins_xf(CPUState *state, uint8_t *code) {
  int cycle = 0;
  switch((code[0] & 0xf0) >> 4) {
    case 0x0: break;
    case 0x1: break;
    case 0x2: break;
    case 0x3: break;
    case 0x4: break;
    case 0x5: break;
    case 0x6: break;
    case 0x7: break;
    case 0x8: break;
    case 0x9: break;
    case 0xa: break;
    case 0xb: break;
    case 0xc: break;
    case 0xd: break;
    case 0xe: break;
    case 0xf: break;
  }
  return cycle;
}

int tick(CPUState *state) {
  unsigned char *code = &state->memory[state->PC];
  printf("%04x->%s(%02x) ", state->PC, i_names[*code], *code);
  switch (i_length[*code]) {
    case 0:
      printf("\nERR\n"); exit(1);
    case 1:
      printf("\n"); break;
    case 2:
      printf("%02x\n", code[1]); break;
    case 3:
      printf("%02x %02x\n", code[1], code[2]); break;
  }
  switch (code[0] & 0x0f) {
    case 0x00: ins_x0(state, code); break;
    case 0x01: ins_x1(state, code); break;
    case 0x02: ins_x2(state, code); break;
    case 0x03: ins_x3(state, code); break;
    case 0x04: ins_x4(state, code); break;
    case 0x05: ins_x5(state, code); break;
    case 0x06: ins_x6(state, code); break;
    case 0x07: ins_x7(state, code); break;
    case 0x08: ins_x8(state, code); break;
    case 0x09: ins_x9(state, code); break;
    case 0x0A: ins_xa(state, code); break;
    case 0x0B: ins_xb(state, code); break;
    case 0x0C: ins_xc(state, code); break;
    case 0x0D: ins_xd(state, code); break;
    case 0x0E: ins_xe(state, code); break;
    case 0x0F: ins_xf(state, code); break;
  }
  state->PC += i_length[*code];
  return 0;
}

void loadCatridge(CPUState* state, char *filename) {
  FILE *f = fopen(filename, "rb");
  if (f == NULL) {
    printf("error: Couldn't open %s\n", filename);
    exit(1);
  }
  fseek(f, 0L, SEEK_END);
  int fsize = ftell(f); // Calculate file size in bytes
  fseek(f, 0L, SEEK_SET);

  uint8_t *header = malloc(0x10);
  fread(header, 16, 1, f);
  uint8_t flag = header[6] & 0x0f;
  uint8_t F = (flag & 0x08) >> 3;
  uint8_t T = (flag & 0x04) >> 2;
  uint8_t B = (flag & 0x02) >> 1;
  uint8_t M = flag & 0x01;
  printf("Header:\n0-3: %c%c%c%c,\n", header[0], header[1], header[2], header[3]);
  printf("4: %dKB PRG-ROM,\n5: %dKB CHR-ROM,\n", header[4] * 16, header[5] * 8);
  printf("6: Mapper %d, F %d, T %d, B %d, M %d\n", (header[6] & 0xf0) >> 4, F, T, B, M);
  printf("9: PAL? %d\n", header[9]);
  if (header[4] > 2) {
    printf("Catridge not supported");
    exit(1);
  }

  uint16_t a_PRGROM = 0x8000;
  fread(&state->memory[a_PRGROM], header[4] * 16384, 1, &f[(T == 0x01 ? 512 : 0)]);
  state->PC = a_PRGROM;
  fclose(f);
}

int main(int argc, char **argv) {
  CPUState *state = CPUInit();
  loadCatridge(state, argv[1]);

  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    printf("SDL failed to initialize.\n");
    exit(1);
  }
  window = SDL_CreateWindow("redemu", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
  if (window == NULL) {
    printf("Window could not be created.\n");
    exit(1);
  }
  SDL_SetWindowResizable(window, SDL_TRUE);
  renderer = SDL_CreateRenderer(window, -1, 0);
  texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, SCREEN_WIDTH, SCREEN_HEIGHT);

  int quit = 0;
  SDL_Event e;
  while (quit != 1) {
    quit = tick(state);
    while (SDL_PollEvent(&e)) {
      if (e.type == SDL_QUIT) {
        quit = 1;
        break;
      }
    }

    for (int i = 0; i < XLINE * YLINE; i++) {
      //
    }
    SDL_UpdateTexture(texture, NULL, screen, XLINE * sizeof(uint32_t));
    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, texture, NULL, NULL);
    SDL_RenderPresent(renderer);
  }

  SDL_DestroyTexture(texture);
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
  return 0;
}
